import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

import { ReactiveFormsModule } from '@angular/forms';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
		ReactiveFormsModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));
  
    var fixture
	var component  
  
  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
	component = fixture.componentInstance;
	fixture.detectChanges();
  });
  
  it('should create Component', () => {
    expect(component).toBeDefined();
  });
  
/*   it('for HTML element', () => {
	var fieldPrep = fixture.nativeElement;
	console.log(fieldPrep, 'fieldPrep')
  //expect(userName.valid).toBeTruthy();
  }); */
  
  it('should check form click function is working', () => {
	  component.trap();
  })
  
});
