import { Component } from '@angular/core';

import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'testforcicd';
  
  formValues = new FormGroup({
	  'name': new FormControl(''),
	  'password': new FormControl('')
  })
  
  trap(){
	 // console.log(this.formValues);
  }
  
}
