import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntbootstrapComponent } from './intbootstrap.component';

describe('IntbootstrapComponent', () => {
  let component: IntbootstrapComponent;
  let fixture: ComponentFixture<IntbootstrapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntbootstrapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntbootstrapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
