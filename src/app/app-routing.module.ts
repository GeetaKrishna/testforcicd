import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IntbootstrapComponent } from './intbootstrap/intbootstrap.component';

const routes: Routes = [
  {path: 'intbootstrap', component: IntbootstrapComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
